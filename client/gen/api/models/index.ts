/* tslint:disable */
/* eslint-disable */
export * from './ApiDeal';
export * from './ApiDealType';
export * from './ApiDealTypeRequest';
export * from './ApiErrorResponse';
export * from './ApiGetDealsResponse';
export * from './ApiPostDealNewRequest';
export * from './ApiPostDealNewResponse';
