/* tslint:disable */
/* eslint-disable */
/**
 * 
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
/**
 * 
 * @export
 * @interface ApiDeal
 */
export interface ApiDeal {
    /**
     * 
     * @type {string}
     * @memberof ApiDeal
     */
    dealEmployeeRequestAccessToken?: string;
    /**
     * 
     * @type {number}
     * @memberof ApiDeal
     */
    dealEmployeeRequestID?: number;
    /**
     * 
     * @type {boolean}
     * @memberof ApiDeal
     */
    dealEmployerResponseExists?: boolean;
    /**
     * 
     * @type {string}
     * @memberof ApiDeal
     */
    dealTypeEmployeeTitle?: string;
    /**
     * 
     * @type {number}
     * @memberof ApiDeal
     */
    dealTypeID?: number;
    /**
     * 
     * @type {string}
     * @memberof ApiDeal
     */
    dealTypeSalaryPeriod?: string;
    /**
     * 
     * @type {number}
     * @memberof ApiDeal
     */
    dealTypeSalaryRangeFrom?: number;
    /**
     * 
     * @type {number}
     * @memberof ApiDeal
     */
    dealTypeSalaryRangeTo?: number;
    /**
     * 
     * @type {number}
     * @memberof ApiDeal
     */
    salaryMatchState?: number;
}

export function ApiDealFromJSON(json: any): ApiDeal {
    return ApiDealFromJSONTyped(json, false);
}

export function ApiDealFromJSONTyped(json: any, ignoreDiscriminator: boolean): ApiDeal {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'dealEmployeeRequestAccessToken': !exists(json, 'dealEmployeeRequestAccessToken') ? undefined : json['dealEmployeeRequestAccessToken'],
        'dealEmployeeRequestID': !exists(json, 'dealEmployeeRequestID') ? undefined : json['dealEmployeeRequestID'],
        'dealEmployerResponseExists': !exists(json, 'dealEmployerResponseExists') ? undefined : json['dealEmployerResponseExists'],
        'dealTypeEmployeeTitle': !exists(json, 'dealTypeEmployeeTitle') ? undefined : json['dealTypeEmployeeTitle'],
        'dealTypeID': !exists(json, 'dealTypeID') ? undefined : json['dealTypeID'],
        'dealTypeSalaryPeriod': !exists(json, 'dealTypeSalaryPeriod') ? undefined : json['dealTypeSalaryPeriod'],
        'dealTypeSalaryRangeFrom': !exists(json, 'dealTypeSalaryRangeFrom') ? undefined : json['dealTypeSalaryRangeFrom'],
        'dealTypeSalaryRangeTo': !exists(json, 'dealTypeSalaryRangeTo') ? undefined : json['dealTypeSalaryRangeTo'],
        'salaryMatchState': !exists(json, 'salaryMatchState') ? undefined : json['salaryMatchState'],
    };
}

export function ApiDealToJSON(value?: ApiDeal | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'dealEmployeeRequestAccessToken': value.dealEmployeeRequestAccessToken,
        'dealEmployeeRequestID': value.dealEmployeeRequestID,
        'dealEmployerResponseExists': value.dealEmployerResponseExists,
        'dealTypeEmployeeTitle': value.dealTypeEmployeeTitle,
        'dealTypeID': value.dealTypeID,
        'dealTypeSalaryPeriod': value.dealTypeSalaryPeriod,
        'dealTypeSalaryRangeFrom': value.dealTypeSalaryRangeFrom,
        'dealTypeSalaryRangeTo': value.dealTypeSalaryRangeTo,
        'salaryMatchState': value.salaryMatchState,
    };
}

