import {Configuration, DealsApi} from "./gen/api";

const dealApiClient = new DealsApi(new Configuration({
    basePath: "http://localhost:8080",
}));

dealApiClient.apiV1DealsPost({
    postDealNewRequest: {
        dealType: {
            salaryPeriod: "monthly", // 'hourly', 'monthly', 'yearly'
            salaryRangeFrom: 8000,
            salaryRangeTo: 12000,
            employeeTitle: "Senior Golang Developer",
        },
        companyUrl: "https://u8hub.com/companies/example",
        vacancyUrl: "https://u8hub.com/companies/example/vacancies/1/",
        employerUrl: "some-employer",
    },
})
    .then(function (response) {
        console.log(response);
        console.log(JSON.stringify(response));
    })
    .catch(console.error);