package lszyid

import "testing"

func BenchmarkShuffle(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_ = Shuffle(int64(i))
	}
}
