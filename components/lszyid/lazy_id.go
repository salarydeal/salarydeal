package lszyid

import (
	"encoding/binary"
	"math/rand"
)

// https://stackoverflow.com/questions/35371385/how-can-i-convert-an-int64-into-a-byte-array-in-go
func Shuffle(i int64) int64 {
	const int64Bytes = 8

	var buffer = make([]byte, int64Bytes)

	binary.LittleEndian.PutUint64(buffer, uint64(i))

	rand.Shuffle(int64Bytes, func(i, j int) {
		buffer[i], buffer[j] = buffer[j], buffer[i]
	})

	// always positive 😊
	buffer[7] &= 0x7F

	return int64(binary.LittleEndian.Uint64(buffer))
}
