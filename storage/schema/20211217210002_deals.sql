-- +goose Up
-- +goose StatementBegin
CREATE TYPE SALARY_PERIOD AS ENUM ('hourly', 'monthly', 'yearly');

CREATE TABLE deal_types
(
    id                SERIAL PRIMARY KEY,
    salary_period     SALARY_PERIOD            NOT NULL,
    salary_range_from INT                      NOT NULL,
    salary_range_to   INT                      NOT NULL,
    employee_title    VARCHAR                  NOT NULL,
    created_by        INT                      NOT NULL REFERENCES users (id),
    created_at        TIMESTAMP WITH TIME ZONE NOT NULL
);

CREATE TABLE deal_employee_requests
(
    id           SERIAL PRIMARY KEY,
    deal_type_id INT                      NOT NULL REFERENCES deal_types (id),
    company_url  VARCHAR                  NOT NULL,
    vacancy_url  VARCHAR                  NOT NULL,
    employer_url VARCHAR                  NOT NULL,
    access_token BIGINT                   NOT NULL,
    created_by   INT                      NOT NULL REFERENCES users (id),
    created_at   TIMESTAMP WITH TIME ZONE NOT NULL
);

CREATE TABLE deal_employer_responses
(
    id                       SERIAL PRIMARY KEY,
    deal_employee_request_id INT                      NOT NULL UNIQUE REFERENCES deal_employee_requests (id),
    salary_range_from        INT                      NOT NULL,
    salary_range_to          INT                      NOT NULL,
    created_by               INT                      NOT NULL REFERENCES users (id),
    created_at               TIMESTAMP WITH TIME ZONE NOT NULL
);

-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE deal_employer_responses;
DROP TABLE deal_employee_requests;
DROP TABLE deal_types;

DROP TYPE SALARY_PERIOD;
-- +goose StatementEnd
