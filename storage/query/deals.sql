-- name: DealTypes :many
SELECT dt.id,
       dt.salary_range_from,
       dt.salary_range_to,
       dt.employee_title
FROM deal_types dt
         INNER JOIN deal_employee_requests deer ON (dt.id = deer.deal_type_id)
WHERE dt.created_by = @created_by
GROUP BY dt.id
ORDER BY MAX(deer.created_at) DESC;

-- name: DealTypesNew :one
INSERT INTO deal_types (salary_period, salary_range_from, salary_range_to, employee_title, created_by, created_at)
VALUES (@salary_period, @salary_range_from, @salary_range_to, @employee_title, @created_by, @created_at)
RETURNING id;

-- name: DealTypesExists :one
SELECT id
FROM deal_types
WHERE id = @id
  AND created_by = @created_by;

-- name: DealEmployeeRequestsNew :one
INSERT INTO deal_employee_requests (deal_type_id, company_url, vacancy_url, employer_url, access_token, created_by,
                                    created_at)
VALUES (@deal_type_id, @company_url, @vacancy_url, @employer_url, @access_token, @created_by, @created_at)
RETURNING id;

-- name: DealsCount :one
SELECT COUNT(deer.id) AS request_count,
       COUNT(derr.id) AS response_count
FROM deal_employee_requests deer
         LEFT JOIN deal_employer_responses derr ON (deer.id = derr.deal_employee_request_id)
WHERE deer.created_by = @created_by;

-- name: Deals :many
SELECT deer.id                AS deal_employee_request_id,
       deer.access_token      AS deal_employee_request_access_token,
       dt.id                  AS deal_type_id,
       dt.salary_period       AS deal_type_salary_period,
       dt.salary_range_from   AS deal_type_salary_range_from,
       dt.salary_range_to     AS deal_type_salary_range_to,
       dt.employee_title      AS deal_type_employee_title,
       derr.id                AS deal_employer_response_id,
       derr.salary_range_from AS deal_employer_response_salary_range_from,
       derr.salary_range_to   AS deal_employer_response_salary_range_to
FROM deal_employee_requests deer
         INNER JOIN deal_types dt ON (dt.id = deer.deal_type_id)
         LEFT JOIN deal_employer_responses derr ON (deer.id = derr.deal_employee_request_id)
WHERE deer.created_by = @created_by
ORDER BY deer.id DESC;