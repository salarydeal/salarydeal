# Salary Deal

### Getting started

##### First start

```bash
make up
make migration-up
make restart
make view
make view-api-docs
```

##### Restart after code changes

```bash
make restart
```

##### Salary match

```go
package salary

import "math"

func Match(
	eeFrom,
	eeTo,
	erFrom,
	erTo int32,
) int {
	if eeTo == 0 {
		eeTo = math.MaxInt32
	}

	if erTo == 0 {
		erTo = math.MaxInt32
	}

	switch {
	// same
	// 1
	// |**| employee = ee
	// |**| employer = er
	case eeFrom == erFrom && eeTo == erTo:
		return 1

	// touch
	// 2
	// *|
	// *|*
	case eeFrom == erFrom && eeTo < erTo:
		return 2
	// 3
	// *|*
	// *|
	case eeFrom == erFrom && eeTo > erTo:
		return 3
	// 4
	//  |*
	// *|*
	case eeFrom > erFrom && eeTo == erTo:
		return 4
	// 5
	// *|*
	//  |*
	case eeFrom < erFrom && eeTo == erTo:
		return 5

	// different
	// 6
	//   |**
	// **|
	case eeFrom > erTo:
		return 6
	// 7
	// **|
	//   |**
	case eeTo < erFrom:
		return 7

	// intersect
	// 8
	//  |*|*
	// *|*|
	case eeTo > erTo && eeFrom > erFrom && erTo > eeFrom:
		return 8
	// 9
	// *|*|
	//  |*|*
	case eeTo < erTo && eeFrom < erFrom && eeTo > erFrom:
		return 9

	// wrap
	// 10
	//  |*|
	// *|*|*
	case erFrom < eeFrom && eeTo < erTo:
		return 10
	// 11
	// *|*|*
	//  |*|
	case eeFrom < erFrom && erTo < eeTo:
		return 11
	}

	return 0
}
```