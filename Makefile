stage := $(or $(SALARYDEAL_STAGE), dev)

include Makefile.$(stage)

dev-env:
	cp .dev.env .env

production-env:
	cp .production.env .env

up:
	sudo docker-compose up -d

build:
	sudo docker-compose up -d --build

app:
	sudo docker exec -it salarydeal_app sh

run:
	sudo docker exec salarydeal_app go build -o /usr/bin/salarydeal-server
	sudo docker exec salarydeal_app salarydeal-server

stop:
	sudo docker exec salarydeal_app pkill salarydeal-server || echo "salarydeal-server already stopped"

restart:
	sudo docker exec salarydeal_app go build -o /usr/bin/salarydeal-server
	sudo docker exec salarydeal_app pkill salarydeal-server || echo "salarydeal-server already stopped"
	sudo docker exec salarydeal_app salarydeal-server

down:
	sudo docker-compose down

fmt:
	go fmt ./components/... ./models/... ./command/... ./templates/... ./controllers/...

template-install:
	go get -u github.com/valyala/quicktemplate
	go get -u github.com/valyala/quicktemplate/qtc

template-generate:
	qtc -dir=templates -skipLineComments
	git add .

frontend:
	npm run esbuild
	# npm run webpack

merge:
	git checkout staging && git merge --no-ff develop

goose-install:
	go get -u github.com/pressly/goose/v3/cmd/goose

swag-install:
	go get -u github.com/swaggo/swag/cmd/swag

MIGRATION_NAME=$(or $(MIGRATION), init)
migrate-create:
	goose -dir ./storage/schema -table schema_migrations postgres $(POSTGRES_URI) create $(MIGRATION_NAME) sql

migrate-up:
	goose -dir ./storage/schema -table schema_migrations postgres $(POSTGRES_URI) up
migrate-redo:
	goose -dir ./storage/schema -table schema_migrations postgres $(POSTGRES_URI) redo
migrate-down:
	goose -dir ./storage/schema -table schema_migrations postgres $(POSTGRES_URI) down
migrate-reset:
	goose -dir ./storage/schema -table schema_migrations postgres $(POSTGRES_URI) reset
migrate-status:
	goose -dir ./storage/schema -table schema_migrations postgres $(POSTGRES_URI) status

generate-dbs:
	docker run --rm -v $(shell pwd):/src -w /src kjconroy/sqlc generate

generate-docs:
	swag init --parseInternal --parseDependency -o apidocs -g main.go

generate-ts-client:
	# https://openapi-generator.tech/docs/installation/
	# openapi-generator-cli generate -i ./apidocs/swagger.yaml --generator-name typescript-fetch -o gen/api
	# npm run generate-ts-client
	docker run --rm \
		-v $(shell pwd):/local openapitools/openapi-generator-cli generate \
		--skip-validate-spec \
		-g typescript-fetch \
		-i /local/apidocs/swagger.yaml \
		-o /local/client/gen/api

view:
	browse http://localhost:8080

view-api-docs:
	browse http://localhost:8080/apidocs/index.html

go-mod-update:
	go get -u
	go mod tidy
	go mod vendor