package api

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"gitlab.com/salarydeal/salarydeal/components/db"
	"gitlab.com/salarydeal/salarydeal/components/lszyid"
	"gitlab.com/salarydeal/salarydeal/components/salary"
	"gitlab.com/salarydeal/salarydeal/storage/dbs"
	"net/http"
	"strconv"
	"time"
)

const lazyUserID = 1

type DealController struct {
	repository *db.Repository
}

func NewDealController(repository *db.Repository) *DealController {
	return &DealController{repository: repository}
}

type ErrorResponse struct {
	ErrorMessage string `json:"error_message"`
}

type EmptyResponse = struct{}

type DealType struct {
	ID              int32  `json:"id"`
	SalaryRangeFrom int32  `json:"salary_range_from"`
	SalaryRangeTo   int32  `json:"salary_range_to"`
	EmployeeTitle   string `json:"employee_title"`
}

type GetDealTypesResponse []DealType

//
// GetDealTypes handler
//
// @Tags Deals
// @Accept json
// @Produce json
// @Description Get deal types
// @Success 200 {object} GetDealTypesResponse
// @Failure 400 {object} ErrorResponse
// @Failure 500 {object} ErrorResponse
// @Router /api/v1/deal-types [get]
//
func (c *DealController) GetDealTypes(ctx *gin.Context) {
	var dealTypes, err = c.repository.Queries().DealTypes(ctx, lazyUserID)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, &ErrorResponse{
			ErrorMessage: err.Error(),
		})

		return
	}

	var result = make([]DealType, len(dealTypes))
	for i, dealType := range dealTypes {
		result[i] = DealType{
			ID:              dealType.ID,
			SalaryRangeFrom: dealType.SalaryRangeFrom,
			SalaryRangeTo:   dealType.SalaryRangeTo,
			EmployeeTitle:   dealType.EmployeeTitle,
		}
	}

	ctx.JSON(http.StatusOK, result)
}

type DealTypeRequest struct {
	ID              int    `json:"id"`
	SalaryPeriod    string `json:"salary_period"` // one of ('hourly', 'monthly', 'yearly')
	SalaryRangeFrom int    `json:"salary_range_from"`
	SalaryRangeTo   int    `json:"salary_range_to"`
	EmployeeTitle   string `json:"employee_title"`
}

type PostDealNewRequest struct {
	DealType    DealTypeRequest `json:"deal_type"`
	CompanyURL  string          `json:"company_url"`
	VacancyURL  string          `json:"vacancy_url"`
	EmployerURL string          `json:"employer_url"`
}

type PostDealNewResponse struct {
	ID          int32  `json:"id"`
	AccessToken string `json:"access_token"`
}

//
// PostDealsNew handler
//
// @Tags Deals
// @Accept json
// @Produce json
// @Description Create deal
// @Param PostDealNewRequest body PostDealNewRequest true "deal request"
// @Success 201 {object} PostDealNewResponse
// @Failure 400 {object} ErrorResponse
// @Failure 500 {object} ErrorResponse
// @Router /api/v1/deals [post]
//
func (c *DealController) PostDealsNew(ctx *gin.Context) {
	var request PostDealNewRequest
	err := ctx.ShouldBindJSON(&request)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, &ErrorResponse{
			ErrorMessage: err.Error(),
		})

		return
	}

	// validate
	if request.DealType.ID > 0 {
		_, err := c.repository.Queries().DealTypesExists(ctx, dbs.DealTypesExistsParams{
			ID:        int32(request.DealType.ID),
			CreatedBy: lazyUserID,
		})
		if err != nil {
			ctx.JSON(http.StatusBadRequest, &ErrorResponse{
				ErrorMessage: err.Error(),
			})

			return
		}
	} else {
		if request.DealType.SalaryRangeFrom <= 0 {
			ctx.JSON(http.StatusBadRequest, &ErrorResponse{
				ErrorMessage: "salary range from required",
			})

			return
		}

		if request.DealType.SalaryRangeTo <= 0 {
			ctx.JSON(http.StatusBadRequest, &ErrorResponse{
				ErrorMessage: "salary range to required",
			})

			return
		}

		if request.DealType.SalaryRangeFrom > request.DealType.SalaryRangeTo {
			ctx.JSON(http.StatusBadRequest, &ErrorResponse{
				ErrorMessage: "salary range invalid",
			})

			return
		}

		switch dbs.SalaryPeriod(request.DealType.SalaryPeriod) {
		case dbs.SalaryPeriodHourly, dbs.SalaryPeriodMonthly, dbs.SalaryPeriodYearly:
		// fine
		default:
			ctx.JSON(http.StatusBadRequest, &ErrorResponse{
				ErrorMessage: fmt.Sprintf("salary period %q incorrect, use on of %q, %q, %q", dbs.SalaryPeriodHourly, dbs.SalaryPeriodMonthly, dbs.SalaryPeriodYearly),
			})

			return
		}
	}

	var (
		now         = time.Now()
		createdAt   = now.UTC().Truncate(time.Second)
		accessToken = lszyid.Shuffle(now.UnixNano())
		responseID  int32
	)

	err = c.repository.WithTransaction(ctx, func(queries *dbs.Queries) error {
		var dealTypeID int32

		if request.DealType.ID > 0 {
			dealTypeID = int32(request.DealType.ID)
		} else {
			dealTypeID, err = queries.DealTypesNew(ctx, dbs.DealTypesNewParams{
				SalaryPeriod:    dbs.SalaryPeriod(request.DealType.SalaryPeriod),
				SalaryRangeFrom: int32(request.DealType.SalaryRangeFrom),
				SalaryRangeTo:   int32(request.DealType.SalaryRangeTo),
				EmployeeTitle:   request.DealType.EmployeeTitle,
				CreatedBy:       lazyUserID,
				CreatedAt:       createdAt,
			})
			if err != nil {
				return err
			}
		}

		responseID, err = queries.DealEmployeeRequestsNew(ctx, dbs.DealEmployeeRequestsNewParams{
			DealTypeID:  dealTypeID,
			CompanyUrl:  request.CompanyURL,
			VacancyUrl:  request.VacancyURL,
			EmployerUrl: request.EmployerURL,
			AccessToken: accessToken,
			CreatedBy:   lazyUserID,
			CreatedAt:   createdAt,
		})
		if err != nil {
			return err
		}

		return nil
	})
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, &ErrorResponse{
			ErrorMessage: err.Error(),
		})

		return
	}

	ctx.JSON(http.StatusCreated, &PostDealNewResponse{
		ID:          responseID,
		AccessToken: strconv.FormatInt(accessToken, 16),
	})
}

type Deal struct {
	DealEmployeeRequestID          int32
	DealTypeID                     int32
	DealTypeSalaryPeriod           string
	DealTypeSalaryRangeFrom        int32
	DealTypeSalaryRangeTo          int32
	DealTypeEmployeeTitle          string
	DealEmployeeRequestAccessToken string
	DealEmployerResponseExists     bool
	SalaryMatchState               int
}

type GetDealsResponse struct {
	Items              []Deal `json:"items"`
	RequestTotalCount  int64  `json:"request_total_count"`
	ResponseTotalCount int64  `json:"response_total_count"`
}

//
// GetDeals handler
//
// @Tags Deals
// @Accept json
// @Produce json
// @Description Get deals
// @Success 200 {object} GetDealsResponse
// @Failure 500 {object} ErrorResponse
// @Router /api/v1/deals [get]
//
func (c *DealController) GetDeals(ctx *gin.Context) {
	var queries = c.repository.Queries()

	count, err := queries.DealsCount(ctx, lazyUserID)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, &ErrorResponse{
			ErrorMessage: err.Error(),
		})

		return
	}

	source, err := queries.Deals(ctx, lazyUserID)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, &ErrorResponse{
			ErrorMessage: err.Error(),
		})

		return
	}

	var result = make([]Deal, len(source))
	for i, deal := range source {
		var (
			accessToken                string
			salaryMatchState           int
			dealEmployerResponseExists = deal.DealEmployerResponseID.Valid
		)

		if dealEmployerResponseExists {
			accessToken = ""

			salaryMatchState = salary.Match(
				deal.DealTypeSalaryRangeFrom,
				deal.DealTypeSalaryRangeTo,
				deal.DealEmployerResponseSalaryRangeFrom.Int32,
				deal.DealEmployerResponseSalaryRangeTo.Int32,
			)
		} else {
			accessToken = strconv.FormatInt(deal.DealEmployeeRequestAccessToken, 16)
		}

		result[i] = Deal{
			DealEmployeeRequestID:          deal.DealEmployeeRequestID,
			DealTypeID:                     deal.DealTypeID,
			DealTypeSalaryPeriod:           string(deal.DealTypeSalaryPeriod),
			DealTypeSalaryRangeFrom:        deal.DealTypeSalaryRangeFrom,
			DealTypeSalaryRangeTo:          deal.DealTypeSalaryRangeTo,
			DealTypeEmployeeTitle:          deal.DealTypeEmployeeTitle,
			DealEmployeeRequestAccessToken: accessToken,
			DealEmployerResponseExists:     dealEmployerResponseExists,
			SalaryMatchState:               salaryMatchState,
		}
	}

	ctx.JSON(http.StatusCreated, &GetDealsResponse{
		Items:              result,
		RequestTotalCount:  count.RequestCount,
		ResponseTotalCount: count.ResponseCount,
	})
}
