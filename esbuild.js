require("esbuild").buildSync({
    entryPoints: [
        "./client/dealsalary-app.ts",
    ],
    bundle: true,
    minify: true,
    outdir: "./public/assets/js",
});