package main

import (
	"github.com/gin-gonic/gin"
	swaggoFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	_ "gitlab.com/salarydeal/salarydeal/apidocs"
	"gitlab.com/salarydeal/salarydeal/components/db"
	"gitlab.com/salarydeal/salarydeal/components/env"
	"gitlab.com/salarydeal/salarydeal/controllers/api"
	"log"
)

func main() {
	var (
		pgCredentials = env.Must("POSTGRES_DSN")
		httpPort      = env.Must("PORT")
	)

	var pgConnection = db.MustConnection(pgCredentials)
	defer pgConnection.Close()

	var repository = db.MustRepository(pgConnection)
	defer repository.Close()

	var r = gin.New()

	{
		r.GET("/apidocs/*any", ginSwagger.WrapHandler(swaggoFiles.Handler))
	}

	r.StaticFile("/", "./public/index.html")
	r.Static("/assets", "./public/assets")

	{
		var dealController = api.NewDealController(repository)

		r.POST("/api/v1/deals", dealController.PostDealsNew)
		r.GET("/api/v1/deal-types", dealController.GetDealTypes)
	}

	var serverErr = r.Run(":" + httpPort) // listen and serve on 0.0.0.0:8080 (for windows "localhost:8080")
	if serverErr != nil {
		log.Fatalln(serverErr)

		return
	}
}
